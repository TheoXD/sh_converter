import json
import datetime, time

from src.json2xml import Json2xml
import dict2xml

import argparse
argparser = argparse.ArgumentParser()

argparser.add_argument('--in', dest='input_file', default="input.old")
argparser.add_argument('--out', dest='output_file', default="output.new")

from enum import Enum
from pathlib import Path

class SerializableObject(json.JSONEncoder):

    def Serialize(self):
        return json.dumps(self, default=self.serializer, sort_keys=False, indent=4)
    
    def serializer(self, object):
        return object.__dict__


#P contains <first_name> and <last_name> fields
class PersonalInfo(SerializableObject):
    first_name : str
    last_name : str

    def __init__(self, _first_name : str = "", _last_name : str = ""):
        self.first_name = _first_name
        self.last_name = _last_name
    
#A contains <street>, <city> and <zip_code>
class Address(SerializableObject):
    street : str
    city : str
    zip_code : int

    def __init__(self, _street : str = "", _city : str = "", _zip_code : int = 0):
        self.street = _street
        self.city = _city
        if _zip_code != None: self.zip_code = _zip_code

#T includes <mobile> and <landline>
class Contact(SerializableObject):
    #mobile_country_code : str
    mobile : str
    #landline_country_code : str
    landline : str

    def __init__(self, _mobile : str = "", _landline : str = ""):
        self.mobile = self.ParsePhoneNumber(_mobile)
        #self.mobile_country_code = self.DetectCountryCode(_mobile)

        self.landline = self.ParsePhoneNumber(_landline)
        #self.landline_country_code = self.DetectCountryCode(_landline)
    
    #def DetectCountryCode(self, _phone_number : str) -> str:
    #    return "SE"
    
    def ParsePhoneNumber(self, _phone_number : str) -> str:
        #return _phone_number.replace("-", "").replace(" ", "")
        return _phone_number

# contains <name> and <born> tags
class FamilyMemberInfo(SerializableObject):
    name : str
    born : int

    def __init__(self, _name : str = "", _born : str = ""):
        self.name = _name
        self.born = self.ParseDate(_born)

    def ParseDate(self, _date : str) -> int:
        date = datetime.datetime(year=int(_date), month=1, day=1)
        return int(time.mktime(date.timetuple()))

#F data inside of <family></family>, like <info>, <address> and <phone>
class FamilyMember(SerializableObject):
    info : FamilyMemberInfo
    address : Address
    phone : Contact

    def __init__(self, _info : FamilyMemberInfo = None, _address : Address = None, _phone : Contact = None):
        if _info != None: self.info = _info
        if _address != None: self.address = _address
        if _phone != None: self.phone = _phone

#P data inside of <person></person>, namely <info>, <phone>, <address> and <family>
class Person(SerializableObject):
    #P
    info : PersonalInfo
    #T
    phone : Contact
    #A
    address : Address
    #F
    family = []

    def __init__(self, _info : PersonalInfo = None, _phone : Contact = None, _address : Address = None, _family = []):
        if _info != None: self.info = _info
        if _phone != None: self.phone = _phone
        if _address != None: self.address = _address
        if _family != None: self.family = _family

# wraps personal data such as <info>, <phone>, <address> and <family> around a <person></person> tag
class PersonWrapper(SerializableObject):
    person : Person

    def __init__(self, _person : Person = None):
        self.person = _person



class ExportFormat(Enum):
    JSON = 0
    XML = 1

class BasicDatabase(SerializableObject):
    people : list

    def __init__(self, _import_file_path: str):
        self.people = []
        self.Import(_import_file_path)

    def AddPerson(self, _person : Person):
        self.people.append(PersonWrapper(_person))
    
    def Import(self, _filename : str):
        with open(_filename, "r") as input_file:

            cursor = None
            p_cursor = None

            for line_nr ,line in enumerate(input_file):
                OPCODE = line[:1]
                imported_data = line[2:].rstrip().split("|")

                if OPCODE == "P": # Found person
                    if p_cursor != None:
                        self.AddPerson(p_cursor)

                    info = PersonalInfo(imported_data[0], imported_data[1])

                    cursor = Person(info)

                    p_cursor = cursor

                
                if OPCODE == "T": # Found phone number
                    imported_phone_info = Contact(imported_data[0], imported_data[1])
                    cursor.phone = imported_phone_info

                if OPCODE == "A": # Found address
                    zip_code = None
                    if len(imported_data) > 2:
                        zip_code = imported_data[2]
                    
                    imported_address = Address(imported_data[0], imported_data[1], zip_code)
                    cursor.address = imported_address

                if OPCODE == "F": # Found family member
                    info = FamilyMemberInfo(imported_data[0], imported_data[1])
                    family_person = FamilyMember(info)

                    cursor = family_person
                    p_cursor.family.append(family_person)
            
            self.AddPerson(p_cursor)        

                
    def Export(self, _destination_file_path: str = None, _format : ExportFormat = ExportFormat.JSON):
        _json_out = self.Serialize()

        if (_format == ExportFormat.JSON):
            if _destination_file_path:
                with open(_destination_file_path, "w") as output_file:
                    json.dump(_json_out, output_file)
            else:
                return _json_out

        if (_format == ExportFormat.XML):
            if _destination_file_path:
                xml_data = Json2xml.fromstring(_json_out)
                _xml_out = dict2xml.dict2xml(xml_data.data, indent="  ")

                with open(_destination_file_path, "w") as output_file:
                    output_file.write(_xml_out)
            else:
                return _xml_out

        

def DetectFileType(_path : Path) -> ExportFormat:
    if _path.suffix == ".xml":
        return ExportFormat.XML
    return ExportFormat.JSON

def Convert(old_file_path, new_file_path):
    basic_database = BasicDatabase(old_file_path)

    _xml_data = basic_database.Export(new_file_path, _format=DetectFileType(Path(new_file_path)) )

def main(args):
    Convert(args.input_file, args.output_file)

if __name__ == '__main__':
    args = argparser.parse_args()
    main(args)